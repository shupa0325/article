package article

import (
	"database/sql"
)

// Article 是文章的資料庫規格
type Article struct {
	Content string
	Author  string
}

// SQLDriver 用來區別本次寫入所需的sql driver
type SQLDriver interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
}

// Save 用來儲存要新增的文章
func Save(a Article, driver SQLDriver) (sql.Result, error) {

	result, err := driver.Exec("INSERT INTO user_info (content, author) VALUES (?, ?)", a.Content, a.Author)
	return result, err
}
